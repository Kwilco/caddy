[![pipeline status](https://gitlab.com/Kwilco/caddy/badges/master/pipeline.svg)](https://gitlab.com/Kwilco/caddy/commits/master)

# 1. Caddy binary

A binary of the Caddy webserver, built directly from the unmodified
[Caddy project](https://github.com/mholt/caddy) official source code.

The binary from the latest pipeline is available for direct download:

https://gitlab.com/Kwilco/caddy/-/jobs/artifacts/master/raw/caddy?job=caddy


# 2. Caddy Docker container

A Docker container of caddy with the Git and DigitalOcean plugins
installed. Available at: `registry.gitlab.com/kwilco/caddy`